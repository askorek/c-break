﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace @break
{
    public partial class MainForm : Form
    {
        private DateTime currentTime = new DateTime();
        private DateTime nextWork = new DateTime();
        private DateTime nextBreak = new DateTime();
        private Stopwatch stoper = new Stopwatch();

        private bool isInWorkMode = false;
        private bool isInBreakMode = false;
        private bool isInAlarmMode = false;

        private TimeSpan leftToBreak;
        private TimeSpan leftToWork;


        public MainForm()
        {
            InitializeComponent();
            this.SizeChanged += new EventHandler(MainForm_sizeeventhandler);
            timer1.Start();
            stoper.Start();
            gotoWork();            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {                    
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            currentTime = DateTime.Now;
            TimeElapsedLabel.Text = stoper.Elapsed.ToString("hh\\:mm\\:ss");
            leftToBreak = nextBreak - currentTime;
            leftToWork = nextWork - currentTime;
            if (isInWorkMode)
            {
                TimeLeftLabel.Text = "Do przerwy: " + leftToBreak.ToString("hh\\:mm\\:ss");
                TrayIcon.Text = "Do przerwy: " + leftToBreak.ToString("hh\\:mm\\:ss");
            }
            else if (isInBreakMode)
            {
                TimeLeftLabel.Text = "Do pracy: " + leftToWork.ToString("hh\\:mm\\:ss");
                TrayIcon.Text = "Koniec przerwy za: " + leftToWork.ToString("mm\\:ss");
            }

            if ((isInWorkMode) & (currentTime.Hour == nextBreak.Hour) & (currentTime.Minute == nextBreak.Minute) & (currentTime.Second == nextBreak.Second))
                breakInfo();

            if ((isInBreakMode) & (currentTime.Hour == nextWork.Hour) & (currentTime.Minute == nextWork.Minute) & (currentTime.Second == nextWork.Second))
               workInfo();

            
        }

        private void PostponeButton_Click(object sender, EventArgs e)
        {
            if (isInAlarmMode & isInWorkMode) workPostpone();
            else if (isInAlarmMode & isInBreakMode) breakPostpone();
            
        }

        private void PostponeBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TimeElapsedLabel_Click(object sender, EventArgs e)
        {

        }

        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if (isInWorkMode) gotoBreak();
            else if (isInBreakMode) gotoWork();
        }

        private void pokażToolStripMenuItem_Click(object sender, EventArgs e) {
            this.Show();
            TrayIcon.Visible = false;
        
        
        }

        void gotoWork() {
            nextBreak = DateTime.Now.AddMinutes(120);
            isInWorkMode = true;
            isInBreakMode = false;
            isInAlarmMode = false;
            PostponeButton.Visible = false;
            PostponeBox.Visible = false;
            AlarmLabel.ForeColor = Color.Black;
            AlarmLabel.Text = "PRACA";
            TimeElapsedLabel.ForeColor = Color.Black;
            stoper.Restart();
            ConfirmButton.Text = "PRZERWA";
            TimeLeftLabel.Visible = true;
         }

        void gotoBreak() {
            nextWork = DateTime.Now.AddMinutes(10);
            isInWorkMode = false;
            isInBreakMode = true;
            isInAlarmMode = false;
            stoper.Restart();
            PostponeButton.Visible = false;
            PostponeBox.Visible = false;
            AlarmLabel.ForeColor = Color.Green;
            AlarmLabel.Text = "PRZERWA";
            nextWork = DateTime.Now.AddMinutes(15);
            TimeElapsedLabel.ForeColor = Color.Green;
            ConfirmButton.Text = "PRACUJ";
            TimeLeftLabel.Visible = true;
         
        }

        void breakInfo() {
            this.WindowState = FormWindowState.Normal;
            AlarmLabel.ForeColor = Color.Red;
            TimeLeftLabel.Visible = false;
            AlarmLabel.Text = "CZAS NA PRZERWĘ !!!";
            TimeElapsedLabel.ForeColor = Color.Red;
            isInAlarmMode = true;
            PostponeButton.Visible = true;
            PostponeBox.Visible = true;
            PostponeBox.SelectedIndex = 0;
            this.Show();
            this.TopMost = true;
        }

        void workInfo() {
            this.WindowState = FormWindowState.Normal;
            isInAlarmMode = true;
            AlarmLabel.Text = "DO ROBOTY";
            TimeElapsedLabel.ForeColor = Color.Red;
            AlarmLabel.ForeColor = Color.Red;
            PostponeButton.Visible = true;
            PostponeBox.Visible = true;
            PostponeBox.SelectedIndex = 0;
            TimeLeftLabel.Visible = false;
            this.Show();
            this.TopMost = true;
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {}

        private void MainForm_sizeeventhandler(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.ShowInTaskbar = false;
                TrayIcon.Visible = true;
                this.Hide();
            }
        }

        private void workPostpone() 
        {
            this.TopMost = false;
            isInAlarmMode = false;
            AlarmLabel.Text = "PRACUJE";
            PostponeButton.Visible = false;
            PostponeBox.Visible = false;
            AlarmLabel.ForeColor = Color.Black;

            int minuteWait = 0;
            if (PostponeBox.Text == "5 min") minuteWait = 5;
            if (PostponeBox.Text == "15 min") minuteWait = 15;
            if (PostponeBox.Text == "30 min") minuteWait = 30;

            TimeLeftLabel.Visible = true;
            nextBreak = DateTime.Now.AddMinutes(minuteWait);
            this.WindowState = FormWindowState.Minimized;
        }

        private void breakPostpone()
        {
            // this.TopMost = false;
            isInAlarmMode = false;
            AlarmLabel.Text = "PRZERWA";
            PostponeButton.Visible = false;
            PostponeBox.Visible = false;
            AlarmLabel.ForeColor = Color.Green;

            int minuteWait = 0;
            if (PostponeBox.Text == "5 min") minuteWait = 5;
            if (PostponeBox.Text == "15 min") minuteWait = 10;
            if (PostponeBox.Text == "30 min") minuteWait = 15;

            TimeLeftLabel.Visible = true;
            nextWork = DateTime.Now.AddMinutes(minuteWait);
            //this.WindowState = FormWindowState.Minimized;
        
        
        }


        private void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.TopMost = true;
            this.WindowState = FormWindowState.Normal;
            TrayIcon.Visible = false;
            this.ShowInTaskbar = true;
            this.Show();
        }


        
    }
}
