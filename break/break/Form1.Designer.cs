﻿namespace @break
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.PostponeButton = new System.Windows.Forms.Button();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.AlarmLabel = new System.Windows.Forms.Label();
            this.PostponeBox = new System.Windows.Forms.ComboBox();
            this.TimeLeftLabel = new System.Windows.Forms.Label();
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.TrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pokażToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.przerwaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zamknijToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TimeElapsedLabel = new System.Windows.Forms.Label();
            this.TrayMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // PostponeButton
            // 
            this.PostponeButton.Location = new System.Drawing.Point(3, 80);
            this.PostponeButton.Name = "PostponeButton";
            this.PostponeButton.Size = new System.Drawing.Size(55, 23);
            this.PostponeButton.TabIndex = 0;
            this.PostponeButton.Text = "czekaj";
            this.PostponeButton.UseVisualStyleBackColor = true;
            this.PostponeButton.Click += new System.EventHandler(this.PostponeButton_Click);
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(109, 66);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(75, 23);
            this.ConfirmButton.TabIndex = 1;
            this.ConfirmButton.Text = "PRACUJ";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // AlarmLabel
            // 
            this.AlarmLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AlarmLabel.Font = new System.Drawing.Font("Impact", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.AlarmLabel.Location = new System.Drawing.Point(0, 0);
            this.AlarmLabel.Name = "AlarmLabel";
            this.AlarmLabel.Size = new System.Drawing.Size(184, 112);
            this.AlarmLabel.TabIndex = 2;
            this.AlarmLabel.Text = "PRACUJE";
            this.AlarmLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.AlarmLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // PostponeBox
            // 
            this.PostponeBox.FormattingEnabled = true;
            this.PostponeBox.Items.AddRange(new object[] {
            "5 min",
            "15 min",
            "30 min"});
            this.PostponeBox.Location = new System.Drawing.Point(8, 53);
            this.PostponeBox.Name = "PostponeBox";
            this.PostponeBox.Size = new System.Drawing.Size(50, 21);
            this.PostponeBox.TabIndex = 3;
            this.PostponeBox.SelectedIndexChanged += new System.EventHandler(this.PostponeBox_SelectedIndexChanged);
            // 
            // TimeLeftLabel
            // 
            this.TimeLeftLabel.AutoSize = true;
            this.TimeLeftLabel.Location = new System.Drawing.Point(79, 90);
            this.TimeLeftLabel.Name = "TimeLeftLabel";
            this.TimeLeftLabel.Size = new System.Drawing.Size(83, 13);
            this.TimeLeftLabel.TabIndex = 4;
            this.TimeLeftLabel.Text = "czas do przerwy";
            // 
            // TrayIcon
            // 
            this.TrayIcon.BalloonTipText = "xxx";
            this.TrayIcon.ContextMenuStrip = this.TrayMenu;
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "notifyIcon1";
            this.TrayIcon.Visible = true;
            this.TrayIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.TrayIcon_MouseDoubleClick);
            // 
            // TrayMenu
            // 
            this.TrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pokażToolStripMenuItem,
            this.przerwaToolStripMenuItem,
            this.zamknijToolStripMenuItem});
            this.TrayMenu.Name = "TrayMenu";
            this.TrayMenu.Size = new System.Drawing.Size(118, 70);
            // 
            // pokażToolStripMenuItem
            // 
            this.pokażToolStripMenuItem.Name = "pokażToolStripMenuItem";
            this.pokażToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.pokażToolStripMenuItem.Text = "Pokaż";
            // 
            // przerwaToolStripMenuItem
            // 
            this.przerwaToolStripMenuItem.Name = "przerwaToolStripMenuItem";
            this.przerwaToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.przerwaToolStripMenuItem.Text = "Przerwa";
            // 
            // zamknijToolStripMenuItem
            // 
            this.zamknijToolStripMenuItem.Name = "zamknijToolStripMenuItem";
            this.zamknijToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.zamknijToolStripMenuItem.Text = "Zamknij";
            this.zamknijToolStripMenuItem.Click += new System.EventHandler(this.zamknijToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TimeElapsedLabel
            // 
            this.TimeElapsedLabel.Location = new System.Drawing.Point(35, 37);
            this.TimeElapsedLabel.Name = "TimeElapsedLabel";
            this.TimeElapsedLabel.Size = new System.Drawing.Size(113, 13);
            this.TimeElapsedLabel.TabIndex = 5;
            this.TimeElapsedLabel.Text = "Pracujesz od 01:02:03";
            this.TimeElapsedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.TimeElapsedLabel.Click += new System.EventHandler(this.TimeElapsedLabel_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(184, 112);
            this.Controls.Add(this.TimeElapsedLabel);
            this.Controls.Add(this.TimeLeftLabel);
            this.Controls.Add(this.PostponeBox);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.PostponeButton);
            this.Controls.Add(this.AlarmLabel);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.TrayMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PostponeButton;
        private System.Windows.Forms.Button ConfirmButton;
        private System.Windows.Forms.Label AlarmLabel;
        private System.Windows.Forms.ComboBox PostponeBox;
        private System.Windows.Forms.Label TimeLeftLabel;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.ContextMenuStrip TrayMenu;
        private System.Windows.Forms.ToolStripMenuItem pokażToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem przerwaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zamknijToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label TimeElapsedLabel;
    }
}

